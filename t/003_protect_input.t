use Test::More tests => 4;
use strict;
use warnings;

# the order is important
use Cotize::ProtectUtils qw/protect_hash project/;

cmp_ok( protect('toto'),    '==',  'toto', "pas de modification" );
cmp_ok( protect('to.to'),   '==',  'toto', "retrait des ." );
cmp_ok( protect('<to.to>'), '==',  'toto', "retrait des souflets" );
cmp_ok( protect('(to.to)'), '==',  'toto', "retrait des parenthèses" );
