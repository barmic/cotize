# Licence Public Barmic
# copyright 2014 Michel Barret <michel.barret@gmail.com>
package Cotize::Model::Types;

use Moose;
use Moose::Util::TypeConstraints;
use Email::Valid;

use Dancer::Plugin::EscapeHTML;
use Dancer ':syntax';

subtype 'Amount',
        as 'Num',
        where { $_ >= 0 && /^[0-9]+(\.[0-9]{0,2})?$/ },
        message { "$_ is not valid amount!" };

subtype 'ContribAmount',
        as 'Amount',
        where { $_ > 0 },
        message { "$_ is not valid amount of contribution!" };

subtype 'Mail',
        as 'Str',
        where {
          Email::Valid->address( -address => $_,
                                 -mxcheck => 1 )
        },
        message { "$_ is not valid mail!" };

subtype 'Hash',
        as 'Str',
        where { $_ =~ /^[^<>_]*$/ };

#subtype 'CleanStr',
#        as 'Str',
#        where { $_ !~ /\</ && $_ !~ /\>/ };

coerce 'Hash',
       from 'Str',
       via {
         $_ =~ s/</\&lt\;/isg;
         $_ =~ s/>/\&gt\;/isg;
         $_ =~ s/_//isg;
         return $_;
       };

#coerce 'CleanStr',
#       from 'Str',
#       via {
#         return escape_html($_);
#       };

coerce 'Amount',
       from 'Num',
       via {
         /^([0-9]+\.[0-9]{0,2})[0-9]*$/;
         return $1;
       };
