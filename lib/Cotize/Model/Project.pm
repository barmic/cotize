# Licence Public Barmic
# copyright 2014 Michel Barret <michel.barret@gmail.com>
package Cotize::Model::Project;

use Moose;
use Dancer ':syntax';

use List::Util qw/reduce/;

use Cotize::Model::Types;

use Digest::SHA qw/hmac_sha256_hex/;

has [ 'name', 'author', 'description' ] => (
   is => 'rw',
   isa => 'Str',
   required => 1);
has duedate => ( is => 'rw', isa => 'Date');
has [ 'see_contributors', 'see_nb_contibutors', 'allow_comment' ] => (
   is => 'rw',
   isa => 'Bool',
   required => 1,
   default => 0);
has contributors => (
   is => 'rw',
   isa => 'ArrayRef[Cotize::Model::Contribution]',
   required => 1,
   default => sub { return [ ] });
has gifts => (
   is => 'rw',
   isa => 'ArrayRef[Cotize::Model::Gift]',
   required => 1,
   default => sub { return [ ] });

has email => ( is => 'rw', isa => 'Mail', required => 1);

has public_hash => (
   is => 'rw',
   isa => 'Str',
);
has admin_hash => (
   is => 'rw',
   isa => 'Str',
);

has amount => (
               is => 'ro',
               isa => 'Amount',
               builder => '_amount',
               lazy => 1
              );
sub BUILD {
   my $self = shift;
   $self->public_hash(hmac_sha256_hex($self->name, config->{secret}));
   $self->admin_hash(hmac_sha256_hex($self->name, config->{secret_admin}));
}

sub _amount {
   my $self = shift;
   my $amount = 0;
   debug 'amount = '.$amount;
   foreach (@{$self->contributors}) {
     $amount += $_->amount;
     debug 'amount = '.$amount;
   }
   return $amount;
}

1;
