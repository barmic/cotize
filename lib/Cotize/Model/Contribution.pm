# Licence Public Barmic
# copyright 2014 Michel Barret <michel.barret@gmail.com>
package Cotize::Model::Contribution;

use Moose;

use Dancer ':syntax';
use Digest::SHA qw/hmac_sha256_hex/;

use Cotize::Model::Types;

has name => ( is => 'rw', isa => 'Str', required => 1);
has email => ( is => 'rw', isa => 'Mail', required => 1);
has amount => ( is => 'rw', isa => 'ContribAmount', required => 1);
has isPayed => ( is => 'rw', isa => 'Bool', required => 1);
has hash => ( is => 'rw', isa => 'Str', required => 0);

sub BUILD {
   my $self = shift;
   $self->hash(hmac_sha256_hex($self->name, config->{secret}));
}

sub toggle {
   my $self = shift;
   $self->isPayed($self->isPayed ? 0 : 1);
}

1;

