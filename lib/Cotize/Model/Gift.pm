# Licence Public Barmic
# copyright 2014 Michel Barret <michel.barret@gmail.com>
package Cotize::Model::Gift;

use Moose;

use Cotize::Model::Types;

has [ 'name', 'description' ] => (is => 'rw', isa => 'Str', required => 1);
has amount => (is => 'rw', isa => 'Amount', required => 1);

1;
