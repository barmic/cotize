package Cotize::Users;

use Exporter;
our @ISA = qw(Exporter);

use strict;
use warnings;
use Exporter qw(import);

use Dancer ':syntax';
use Digest::SHA qw/sha256/;

our $VERSION = '0.1';
our @EXPORT_OK = qw/auth/;

sub auth {
   my ($config, $login, $passwd) = @_;
   open(my $data, '<', $config) or die 'Could not open '.$config.' '.$!."\n";
   my $hash = unpack("H*", sha256($passwd));

   my $auth = false;
   foreach (<$data>) {
      chomp;
      my @fields = split ';';
      $auth = $login eq $fields[0] && $hash eq $fields[1];
   }
   return $auth;
}

true;
