# Licence Public Barmic
# copyright 2014 Michel Barret <michel.barret@gmail.com>
package Cotize::Mail;

use Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw/sendmail/;

use strict;
use warnings;

use Dancer ':syntax';
use Dancer::Plugin::EscapeHTML;

use Template;

use YAML::XS qw/LoadFile/;;

sub sendmail {
  my ($destmail, $templatename, $vars) = @_;
  my $srcmail  =  config->{email};
  my ($subjecttt, $bodytt) = LoadFile(config->{mail_dir}.'/'.$templatename);

  my $config = {
        INTERPOLATE  => 1,               # expand "$var" in plain text
        POST_CHOMP   => 1,               # cleanup whitespace
        EVAL_PERL    => 1,               # evaluate Perl code blocks
    };

  my $tt = Template->new($config) or die $Template::ERROR, "\n";

  my ($subject, $body);
  $tt->process(\$subjecttt, $vars, \$subject) or die $tt->error(), "\n";
  $tt->process(\$bodytt, $vars, \$body)    or die $tt->error(), "\n";

  my $msg = MIME::Lite->new(From     => $srcmail,
                            To       => $destmail,
                            Subject  => $subject,
                            Data     => $body
                           );

  $msg->send;
}
