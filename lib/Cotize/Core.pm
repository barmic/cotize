# Licence Public Barmic
# copyright 2014 Michel Barret <michel.barret@gmail.com>
package Cotize::Core;

use Exporter;
our @ISA = qw(Exporter);

use strict;
use warnings;

use Dancer ':syntax';

use MIME::Lite;

use YAML qw/LoadFile DumpFile/;
use Path::Class;

use Cotize::Mail;
use Cotize::Model::Project;
use Cotize::Model::Contribution;
use Cotize::Store;

our $VERSION = '0.1';
our @EXPORT = qw/read_project create_project contribute_project read_project_by_admin toggle_payment get_contrib update_contrib remove_contrib/;

sub read_project {
   my $hash = dir(shift);
   my ($project, $contribs) = Cotize::Store::read_project $hash;
   $project->see_contributors(0) if (2 < scalar($contribs) or $project->amount >= 50);
   return ($project, $contribs);
}

sub contribute_project {
   my ($hash, $name, $amount, $email) = @_;

   my ($project, $contribs) = Cotize::Store::read_project $hash;

   scalar(grep { $_->name eq $name } @{$project->contributors}) == 0 or die 'Contribution en double ('.$name.')';
   my $contrib = Cotize::Model::Contribution->new(
                                                       name => $name,
                                                       amount => $amount,
                                                       email => $email,
                                                       project => $project,
                                                       isPayed => 0
                                                      );
   my $uuid = Cotize::Store::store_contribution($hash, $contrib);
   my $vars = {
     project => $project->name,
     url_contrib => config->{base_url}.'/contrib/'.$uuid
   };
   sendmail($email, 'validation.yml', $vars);
   return $project;
}

sub create_project {
   my ($name, $author, $description, $nbContrib, $seeContrib, $allowComment, $email, $dueDate) = @_;

   debug 'email => '.$email;
   my $project = Cotize::Model::Project->new(
      name => $name,
      author => $author,
      description => $description,
      see_contributors => 1,
      see_nb_contibutors => $nbContrib,
      allow_comment => $allowComment,
      email => $email
   );

   my $result = Cotize::Store::store_project($project);

   my $vars = {
     project => $project->name,
     url_public => config->{base_url}.'/project/'.$project->public_hash,
     url_admin => config->{base_url}.'/admin/'.$project->admin_hash

   };
   sendmail($email, 'create-project-validation.yml', $vars);

   return $project;
}

sub read_project_by_admin {
    my ($hash) = @_;
    my ($project, $contribs) = Cotize::Store::read_project_by_admin $hash;
    return ($project, $contribs);
}

sub toggle_payment {
    my ($hash, $hash_contrib) = @_;
    my ($project, $contribs) = Cotize::Store::read_project_by_admin($hash);

    for (grep {$_->hash eq $hash_contrib} @{$project->contributors}) {
        $_->toggle();
        Cotize::Store::update_contribution $project->public_hash, $_;
    }
    return $project;
}

sub get_contrib {
    my ($hash) = @_;
    my $project = Cotize::Store::project_of_contribution $hash;
    my $contrib = Cotize::Store::read_contrib $project, $hash;
    return ($project, $contrib);
}

sub update_contrib {
    my ($hash, $new_amount) = @_;
    my $project = Cotize::Store::project_of_contribution $hash;
    my $contrib = Cotize::Store::read_contrib $project, $hash;
    $contrib->amount($new_amount);
    Cotize::Store::update_contribution $project->public_hash, $contrib;
}

sub remove_contrib {
  my ($project_hash, $contrib) = @_;
  my $project = Cotize::Store::read_project_by_admin $project_hash;
  Cotize::Store::delete_contrib $project->public_hash, $contrib;
}

true;
