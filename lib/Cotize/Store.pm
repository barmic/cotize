# Licence Public Barmic
# copyright 2014 Michel Barret <michel.barret@gmail.com>
package Cotize::Store;

use Exporter;
our @ISA = qw(Exporter);

use Dancer ':syntax';
use 5.010;

use strict;
use warnings;

use File::Path qw(make_path remove_tree);
use YAML qw/LoadFile DumpFile/;
use Path::Class;
use File::Glob;
use File::Basename;

use Cotize::Model::Project;
use Cotize::Model::Contribution;

our $VERSION = '0.1';

sub store_project {
  my ($project) = @_;

  my $projdir = config->{data_dir}.'/public/'.$project->public_hash;
  my $proj_admin_dir = config->{data_dir}.'/admin/'.$project->admin_hash;
  mkdir($projdir, 0700);

  return 'Le projet '.$project->name.' existe déjà' if (-f $projdir.'/descriptor.yml');

  my $hash_data = {
    name => $project->name,
    author => $project->author,
    description => $project->description,
    email => $project->email
  };

  my $target = '../public/'.$project->public_hash;
  symlink($target, $proj_admin_dir);

  DumpFile($projdir.'/descriptor.yml', $hash_data);
}

sub read_contrib {
  my ($project, $contrib_hash) = @_;

  my $dir = config->{data_dir};
  my @projdir = glob "$dir/public/*/$contrib_hash.contrib.yml";

  return read_contrib_from_file($projdir[0]);
}

sub read_contrib_from_file {
  my ($filename) = @_;

  my $data = LoadFile($filename) or die 'Impossible de trouver la contribution';

  return Cotize::Model::Contribution->new(
                                                      name    => $data->{name},
                                                      amount  => $data->{amount},
                                                      email   => $data->{email},
                                                      isPayed => $data->{isPayed}
                                                     );
}

sub read_project {
  my ($hash) = @_;

  my $projdir = config->{data_dir}.'/public/'.$hash;
  info 'On charge le projet : '.$projdir.'/descriptor.yml';
  my $data = LoadFile($projdir.'/descriptor.yml') or die 'Le project '.$projdir.'/descriptor.yml'.' nexiste pas';

  my $project = Cotize::Model::Project->new(
                                                 name => $data->{name},
                                                 author => $data->{author},
                                                 description => $data->{description},
                                                 email => $data->{email},
                                                 see_contributors => 1,
                                                 see_nb_contibutors => 0,
                                                 allow_comment => 0
                                                );
  info 'Récupération des contributions';
  my @contribs = map {read_contrib_from_file($_)} glob "$projdir/*.contrib.yml";
  push @{$project->contributors}, $_ for @contribs;

  info 'On a tous';
  return ($project, \@contribs);
}

sub read_project_by_admin {
  my ($hash) = @_;

  my $projdir = config->{data_dir}.'/admin/'.$hash;
  my $data = LoadFile($projdir.'/descriptor.yml') or die 'Le project '.$hash.' n\'existe pas';

  my $project = Cotize::Model::Project->new(
                                                 name => $data->{name},
                                                 author => $data->{author},
                                                 description => $data->{description},
                                                 email => $data->{email},
                                                 see_contributors => 1,
                                                 see_nb_contibutors => 0,
                                                 allow_comment => 0
                                                );
  my @contribs = map {read_contrib_from_file($_)} glob "$projdir/*.contrib.yml";
  push @{$project->contributors}, $_ for @contribs;

  return ($project, \@contribs);
}

sub store_contribution {
  my ($project_hash, $contribution) = @_;

  my $data = {
    name    => $contribution->name,
    amount  => $contribution->amount,
    email   => $contribution->email,
    isPayed => $contribution->isPayed

  };

  my $contrib_path = config->{data_dir}.'/public/'.$project_hash.'/'.$contribution->hash.'.contrib.yml';
  DumpFile($contrib_path, $data);

  return $contribution->hash;
}

sub project_of_contribution {
  my ($hash) = @_;

  my $root = config->{data_dir};

  info 'Recherche dans le chemin : '.$root.'/public/*/'.$hash.'.contrib.yml';

  #my @projects = map {my $f=dirname($_).'/descriptor.yml'; LoadFile($f)} glob "$root/public/*/$hash.contrib.yml";
  my @projects = map {basename(dirname($_))} glob "$root/public/*/$hash.contrib.yml";
  #scalar(@projects) == 1 or die 'Impossible de trouver le projet de la contribution '.$hash.'('.$#projects.')';

  info 'On a trouvé '.$#projects.' projets';

  info 'Chargement du projet '.$projects[0];
  my ($project, $contribs) = read_project $projects[0];
  defined($project) or die "coucou";

  return $project;
}

sub update_contribution {
  my ($project_hash, $contribution) = @_;

  my $data = {
    name    => $contribution->name,
    amount  => $contribution->amount,
    email   => $contribution->email,
    isPayed => $contribution->isPayed
  };

  my $contrib_path = config->{data_dir}.'/public/'.$project_hash.'/'.$contribution->hash.'.contrib.yml';
  unlink $contrib_path;
  DumpFile($contrib_path, $data);

  return $contribution->hash;
}

sub delete_contrib {
  my ($project, $contrib) = @_;
  debug 'unlink '.config->{data_dir}.'/public/'.$project.'/'.$contrib.'.contrib.yml';
  unlink config->{data_dir}.'/public/'.$project.'/'.$contrib.'.contrib.yml';
}

1
