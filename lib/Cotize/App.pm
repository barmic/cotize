# Licence Public Barmic
# copyright 2014 Michel Barret <michel.barret@gmail.com>
package Cotize::App;

use Dancer ':syntax';
use Dancer::Exception qw(:all);
use Dancer::Plugin::EscapeHTML;
use 5.010;

use Cotize::Users;
use Cotize::Core;

our $VERSION = '0.1';
set logger => 'file';

get '/' => sub {
    redirect '/create_project';
};

sub project_to_hash {
    my $project = shift;
    return {
        name             => $project->name,
        description      => $project->description,
        amount           => $project->amount,
        see_contributors => $project->see_contributors,
        public_hash      => $project->public_hash,
        admin_hash       => $project->admin_hash
    };
}

sub contribution_to_hash {
    my $contrib = shift;
    return {
        name    => $contrib->name,
        email   => $contrib->email,
        amount  => $contrib->amount,
        isPayed => $contrib->isPayed,
        hash    => $contrib->hash
    };
}

get '/project/:public_hash' => sub {
   my ($project, $contribs) = read_project param('public_hash');
   my @contributors = map {contribution_to_hash($_)} sort { $a->name cmp $b->name } @{$contribs};
   template 'project', {
      project => project_to_hash($project),
      contributors => \@contributors,
      success => 'hide'
   };
};

post '/project/:hash' => sub {
  my ($user, $amount, $mail) = (param('username'), param('amount'), param('email'));
  my $project = contribute_project param('hash'), $user, $amount, $mail;
  my @contributors = map {contribution_to_hash($_)} sort { $a->name cmp $b->name } @{$project->contributors};

  template 'project', {
      project => project_to_hash($project),
      contributors => \@contributors,
      username => escape_html($user),
      amount => escape_html($amount),
      mail => escape_html($mail)
  };
};

# Admin the project

get '/admin/:admin_hash' => sub {
   my ($project, $contribs) = read_project_by_admin param('admin_hash');
   debug $contribs;
   template 'results', {
      project => project_to_hash($project),
      contributions => $contribs
   };
};

get '/payement/:admin_hash/:contrib_hash' => sub {
   my $project = toggle_payment param('admin_hash'), param('contrib_hash');
   redirect '/admin/'.param('admin_hash');
};

# Create a project

get '/create_project' => sub {
    template 'index';
};

post '/create_project' => sub {
  my ($name, $author, $description, $email) = (param('name'), param('author'), param('description'), param('email'));
  # TODO Création d'une map options pour les options du projet
  my $nbContrib    = param('see_nb_contibutors') // false;
  my $seeContrib   = param('see_contributors') // false;
  my $allowComment = param('allow_comment') // false;

  my $project = create_project $name, $author, $description, $nbContrib eq 'on', $seeContrib eq 'on', $allowComment eq 'on', $email;

  my $vars;
  if ($project->isa('Cotize::Model::Project')) {
      $vars = {
          project => project_to_hash($project),
          base_url => config->{base_url}
      };
  } else {
      $vars = { error_msg => escape_html($project), base_url => config->{base_url} };
  }

  template 'index', $vars;
};

# Edit contribution

get '/contrib/:hash' => sub {
  my ($project, $contrib) = get_contrib param('hash');
  template 'contrib', {
      project => project_to_hash($project),
      contrib => contribution_to_hash($contrib)
  };
};

post '/contrib/:hash/update' => sub {
  update_contrib param('hash'), param('amount');
  redirect '/contrib/'.param('hash');
};

get '/remove/:project_hash/:contrib_hash' => sub {
  remove_contrib param('project_hash'), param('contrib_hash');;

  redirect '/admin/'.param('project_hash');
};

any '/error' => sub {
   'Erreur !!!!!!';
};

true;
