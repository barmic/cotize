#!/usr/bin/env python

from weboob.tools.browser2 import *
import sys

class CreateProjectPage(HTMLPage):
    public_hash = ''
    admin_hash = ''

    def create(self, name, author, description, email):
        form = self.get_form(xpath='//form')
        form['name'] = name
        form['author'] = author
        form['description'] = description
        form['email'] = email
        form.submit()

        links = self.doc.write("/tmp/fichier") #.xpath('//ul')
        print(links)
        public_hash = links[0]
        admin_hash = links[1]

    def get_public_hash(self):
        return public_hash

    def get_admin_hash(self):
        return admin_hash

class Cotize(PagesBrowser):
    BASEURL = 'http://localhost:3000'
    create = URL('/create_project', CreateProjectPage)

if __name__ == '__main__':
    cotize = Cotize()
    creator = cotize.create.go()
    creator.create('Mon projet', 'moi', 'Ma description', 'michel.barret@gmail.com')
    print("public => " + creator.get_public_hash())
    print("admin => " + creator.get_admin_hash())
