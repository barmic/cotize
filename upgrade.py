#!/usr/bin/env python

from fabric.api import *
from fabric.contrib.files import *

env.use_ssh_config = True

def install():
    with cd('/srv/crowgifting'):
        run('git pull')
        if exists('nohup.out'):
            pid=run('awk \'/listening/{print $5}\' nohup.out')
            run('rm nohup.out')
            run('kill ' + pid)
        run('nohup bin/app.pl -e production', pty=False)
